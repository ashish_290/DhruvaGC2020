/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tspl.NewCode.Queues;

import com.tspl.NewCode.Interface.Codes;
import java.util.LinkedList;
import java.util.Queue;
import net.wimpi.modbus.net.SerialConnection;

public class ProcessQueue implements Codes{

    Queue<ReadQueue> readQueue = new LinkedList<>(); // Create Queue for Reading Value
    Queue<WriteQueue> writeQueue = new LinkedList<>(); // Create Queue for Writing Value

    public void requestQueue(int slaveId, int functionCode, int startingAddress, int count,int processRequest,SerialConnection con) {
        switch(processRequest)
        {
            case READ_REQUEST:
                readQueue.add(new ReadQueue(slaveId, functionCode, startingAddress, count, con));
                readQueue.poll();
        }
    }

}
