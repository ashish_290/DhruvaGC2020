
package com.tspl.NewCode.Queues;

import com.tspl.NewCode.Utility.EPC;
import com.tspl.NewCode.Utility.FID;
import com.tspl.NewCode.Utility.DTC;
import com.tspl.NewCode.Interface.Codes;
import net.wimpi.modbus.msg.ReadCoilsResponse;
import net.wimpi.modbus.msg.ReadMultipleRegistersResponse;
import net.wimpi.modbus.net.SerialConnection;

public final class ReadQueue implements Codes {

    DTC readDTCData = new DTC();
    EPC readEPCData = new EPC();
    FID readFIDData = new FID();

    static ReadCoilsResponse Coil_Res = null; // the Coil Response
    static ReadMultipleRegistersResponse Reg_Res = null; // the Holding Register Response

    public ReadQueue(int slaveId, int functionCode, int startingAddress, int count, SerialConnection con) {
        if (slaveId >= 1 && slaveId <= 5) {
            if (functionCode == READ_COIL) {
                Coil_Res = readDTCData.processReqResForCoil(slaveId, functionCode, startingAddress, count, con);
                System.out.println("Coil =" + Coil_Res.getCoils().toString());
            } else if (functionCode == READ_MULTIPLE_HOLIDNG_REGISTER) {
                Reg_Res = readDTCData.processReqResForRegister(slaveId, functionCode, startingAddress, count, con);
                for (int n = 0; n < Reg_Res.getWordCount(); n++) {
                    System.out.println("Word " + n + "=" + Reg_Res.getRegisterValue(n));
                }
            }
        } else if (slaveId == 15) {
            if (functionCode == READ_COIL) {
                Coil_Res = readDTCData.processReqResForCoil(slaveId, functionCode, startingAddress, count, con);
                System.out.println("Coil =" + Coil_Res.getCoils().toString());
            } else if (functionCode == READ_MULTIPLE_HOLIDNG_REGISTER) {
                Reg_Res = readDTCData.processReqResForRegister(slaveId, functionCode, startingAddress, count, con);
                for (int n = 0; n < Reg_Res.getWordCount(); n++) {
                    System.out.println("Word " + n + "=" + Reg_Res.getRegisterValue(n));
                }
            }
        } else if (slaveId == 20) {
            if (functionCode == READ_COIL) {
                Coil_Res = readDTCData.processReqResForCoil(slaveId, functionCode, startingAddress, count, con);
                System.out.println("Coil =" + Coil_Res.getCoils().toString());
            } else if (functionCode == READ_MULTIPLE_HOLIDNG_REGISTER) {
                Reg_Res = readDTCData.processReqResForRegister(slaveId, functionCode, startingAddress, count, con);
                for (int n = 0; n < Reg_Res.getWordCount(); n++) {
                    System.out.println("Word " + n + "=" + Reg_Res.getRegisterValue(n));
                }
            }
        }
    }
}
