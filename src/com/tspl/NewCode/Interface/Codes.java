package com.tspl.NewCode.Interface;

public interface Codes {

    public static int READ_COIL = 1;
    public static int READ_DISCRETE_INPUT = 2;
    public static int READ_INPUT_REGISTERS = 3;
    public static int READ_MULTIPLE_HOLIDNG_REGISTER = 4;
    
    public static int WRITE_SINGLE_COIL = 5;
    public static int WRITE_SINGLE_HOLDING_REGISTER = 6;
    public static int WRITE_MULTIPLE_COILS = 15;
    public static int WRITE_MULITPLE_HOLDING_REGISTER = 16;

    public static int READ_REQUEST = 1;
    public static int WRITE_REQUEST = 2;

    public static int SLAVE_ID_DTC_1 = 1;
    public static int SLAVE_ID_DTC_2 = 2;
    public static int SLAVE_ID_DTC_3 = 3;
    public static int SLAVE_ID_DTC_4 = 4;
    public static int SLAVE_ID_DTC_5 = 5;
    public static int DTC_STATING_ADDRESS = 4096;
    public static int DTC_HOLIDING_REGISTER_COUNT = 2;

    public static int SLAVE_ID_FID = 15;
    public static int FID_STATING_ADDRESS = 0;
    public static int FID_HOLIDING_REGISTER_COUNT = 3;

    public static int SLAVE_ID_EPC = 20;
    public static int EPC_PV_STATING_ADDRESS = 192;
    public static int EPC_SV_STATING_ADDRESS = 8;
    public static int EPC_HOLDING_REGISTER_COUNT = 3;
}
