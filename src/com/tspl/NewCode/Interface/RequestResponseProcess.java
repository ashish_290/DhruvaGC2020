/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tspl.NewCode.Interface;

import net.wimpi.modbus.msg.ReadCoilsResponse;
import net.wimpi.modbus.msg.ReadMultipleRegistersResponse;
import net.wimpi.modbus.net.SerialConnection;

public interface RequestResponseProcess {
   
    public ReadCoilsResponse processReqResForCoil(int slaveId, int functionCode, int startingAddress, int count, SerialConnection con);
    public ReadMultipleRegistersResponse processReqResForRegister(int slaveId, int functionCode, int startingAddress, int count, SerialConnection con);
}
