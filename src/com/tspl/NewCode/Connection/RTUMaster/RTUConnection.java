package com.tspl.NewCode.Connection.RTUMaster;

import net.wimpi.modbus.net.SerialConnection;
import net.wimpi.modbus.util.SerialParameters;

public class RTUConnection {

    public SerialConnection con = null; // the connection
    SerialParameters params = new SerialParameters();

    public RTUConnection() {
    }

    
    public RTUConnection(String portName, int baudRate, int databits, String parity, int stopbits,
            String encoding, boolean echo) {
        try {

            params.setPortName(portName);
            params.setBaudRate(baudRate);
            params.setDatabits(databits);
            params.setParity(parity);
            params.setStopbits(stopbits);
            params.setEncoding(encoding);
            params.setEcho(echo);
            con = new SerialConnection(params);
            con.open(); // Open Serial Connection
        } catch (Exception e) {
            System.out.println(e);
        }
    }
    
    public boolean isOpen()
    {
        if(con.isOpen())
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    public SerialConnection ret_con()
    {
        return con;
    }
}
