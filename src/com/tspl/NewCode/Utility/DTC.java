/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tspl.NewCode.Utility;

import com.tspl.NewCode.Interface.Codes;
import com.tspl.NewCode.Interface.RequestResponseProcess;
import net.wimpi.modbus.ModbusException;
import net.wimpi.modbus.ModbusSlaveException;
import net.wimpi.modbus.io.ModbusSerialTransaction;
import net.wimpi.modbus.msg.ReadCoilsRequest;
import net.wimpi.modbus.msg.ReadCoilsResponse;
import net.wimpi.modbus.msg.ReadMultipleRegistersRequest;
import net.wimpi.modbus.msg.ReadMultipleRegistersResponse;
import net.wimpi.modbus.net.SerialConnection;

public class DTC implements RequestResponseProcess, Codes {

    static ModbusSerialTransaction trans = null; // the transaction
    static ReadMultipleRegistersRequest Reg_Req = null; // the Holding Register Request
    static ReadMultipleRegistersResponse Reg_Res = null; // the Holding Register Response

    static ReadCoilsRequest Coil_Req = null; // the Coil Request
    static ReadCoilsResponse Coil_Res = null; // the Coil Response

    SerialConnection con = null;
    
    @Override
    public ReadCoilsResponse processReqResForCoil(int slaveId, int functionCode, int startingAddress, int count, SerialConnection con) {
        // Request for data
        try {
            Coil_Req = new ReadCoilsRequest(startingAddress, count);
            Coil_Req.setUnitID(slaveId);
            Coil_Req.setHeadless();

            // Do Transaction
            trans = new ModbusSerialTransaction(con);
            trans.setRequest(Coil_Req);
            trans.setRetries(1);
            trans.execute();

            // Getting response 
            Coil_Res = (ReadCoilsResponse) trans.getResponse();
           // System.out.println("Coil =" + Coil_Res.getCoils().toString());
        } catch (ModbusSlaveException e) {
            System.out.println(e);
        } catch (ModbusException e) {
            System.out.println(e);
        } 
        return Coil_Res;
    }

    @Override
    public ReadMultipleRegistersResponse processReqResForRegister(int slaveId, int functionCode, int startingAddress, int count, SerialConnection con) {
        // Request for data
        try {
            Reg_Req = new ReadMultipleRegistersRequest(startingAddress, count);
            Reg_Req.setUnitID(slaveId);
            Reg_Req.setHeadless();

            // Do Transaction
            trans = new ModbusSerialTransaction(con);
            trans.setRequest(Reg_Req);
            trans.setRetries(1);
            trans.execute();

            // Getting response 
            Reg_Res = (ReadMultipleRegistersResponse) trans.getResponse();//should return this response

        } catch (ModbusSlaveException e) {
            System.out.println(e);
        } catch (ModbusException e) {
            System.out.println(e);
        }
        return Reg_Res;
    }
}
