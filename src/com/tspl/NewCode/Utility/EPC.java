/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tspl.NewCode.Utility;

import com.tspl.NewCode.Interface.Codes;
import com.tspl.NewCode.Interface.RequestResponseProcess;
import static com.tspl.NewCode.Utility.DTC.Coil_Req;
import static com.tspl.NewCode.Utility.DTC.Coil_Res;
import static com.tspl.NewCode.Utility.FID.Coil_Res;
import net.wimpi.modbus.ModbusException;
import net.wimpi.modbus.ModbusSlaveException;
import net.wimpi.modbus.io.ModbusSerialTransaction;
import net.wimpi.modbus.msg.ReadCoilsRequest;
import net.wimpi.modbus.msg.ReadCoilsResponse;
import net.wimpi.modbus.msg.ReadMultipleRegistersRequest;
import net.wimpi.modbus.msg.ReadMultipleRegistersResponse;
import net.wimpi.modbus.net.SerialConnection;

public class EPC implements RequestResponseProcess, Codes {

    static ModbusSerialTransaction trans = null; // the transaction
    static ReadMultipleRegistersRequest Reg_Req = null; // the request
    static ReadMultipleRegistersResponse Reg_Res = null; // the response

    static ReadCoilsRequest Coil_Req = null;
    static ReadCoilsResponse Coil_Res = null;

    SerialConnection con = null;

    @Override
    public ReadCoilsResponse processReqResForCoil(int slaveId, int functionCode, int startingAddress, int count, SerialConnection con) {
        // Request for data
        try {
            Coil_Req = new ReadCoilsRequest(startingAddress, count);
            Coil_Req.setUnitID(slaveId);
            Coil_Req.setHeadless();

            // Do Transaction
            trans = new ModbusSerialTransaction(con);
            trans.setRequest(Coil_Req);
            trans.setRetries(1);
            trans.execute();

            // Getting response 
            Coil_Res = (ReadCoilsResponse) trans.getResponse();
            // System.out.println("Coil =" + Coil_Res.getCoils().toString());
        } catch (ModbusSlaveException e) {
            System.out.println(e);
        } catch (ModbusException e) {
            System.out.println(e);
        }
        return Coil_Res;
    }

    @Override
    public ReadMultipleRegistersResponse processReqResForRegister(int slaveId, int functionCode, int startingAddress, int count, SerialConnection con) {
        // Request for data
        try {
            Reg_Req = new ReadMultipleRegistersRequest(startingAddress, count);
            Reg_Req.setUnitID(slaveId);
            Reg_Req.setHeadless();

            // Do Transaction
            trans = new ModbusSerialTransaction(con);
            trans.setRequest(Reg_Req);
            trans.setRetries(1);
            trans.execute();

            // Getting response 
            Reg_Res = (ReadMultipleRegistersResponse) trans.getResponse();//should return this response
//            for (int n = 0; n < Reg_Res.getWordCount(); n++) {
//                System.out.println("Word " + n + "=" + Reg_Res.getRegisterValue(n));
//            }
        } catch (ModbusSlaveException e) {
            System.out.println(e);
        } catch (ModbusException e) {
            System.out.println(e);
        }
        return Reg_Res;
    }
}
