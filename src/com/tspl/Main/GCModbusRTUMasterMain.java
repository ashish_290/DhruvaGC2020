package com.tspl.Main;

import com.tspl.NewCode.Connection.RTUMaster.RTUConnection;
import com.tspl.NewCode.Queues.ProcessQueue;
import com.tspl.NewCode.Interface.Codes;
import static com.tspl.NewCode.Interface.Codes.DTC_HOLIDING_REGISTER_COUNT;
import static com.tspl.NewCode.Interface.Codes.DTC_STATING_ADDRESS;
import static com.tspl.NewCode.Interface.Codes.READ_MULTIPLE_HOLIDNG_REGISTER;
import static com.tspl.NewCode.Interface.Codes.READ_REQUEST;
import static com.tspl.NewCode.Interface.Codes.SLAVE_ID_DTC_3;
import static com.tspl.NewCode.Interface.Codes.SLAVE_ID_DTC_4;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GCModbusRTUMasterMain implements Codes {

    public static void main(String[] args) {

        System.setProperty("net.wimpi.modbus.debug", "true");
        System.out.println("debug set");

        RTUConnection rtuConnection = new RTUConnection("/dev/ttyUSB0", 19200, 8, "none", 1, "rtu", false);
        
        ProcessQueue processQueue = new ProcessQueue();

        if (rtuConnection.isOpen()) {
            System.out.println("Connection Done!!!");
        } else {
            System.out.println("No connection Found!!!");
            System.exit(0);
        }

        Timer timer = new Timer();
        timer.schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        try {
                            processQueue.requestQueue(
                                    SLAVE_ID_DTC_3, //slaveid
                                    READ_MULTIPLE_HOLIDNG_REGISTER, //Modbus function code
                                    DTC_STATING_ADDRESS, // Starting Address.
                                    DTC_HOLIDING_REGISTER_COUNT, // Total Read register value
                                    READ_REQUEST, // Request Read or Write
                                    rtuConnection.con);
                            
                            processQueue.requestQueue(
                                    SLAVE_ID_DTC_4, //slaveid
                                    READ_MULTIPLE_HOLIDNG_REGISTER, //Modbus function code
                                    DTC_STATING_ADDRESS, // Starting Address.
                                    DTC_HOLIDING_REGISTER_COUNT, // Total Read register value
                                    READ_REQUEST, // Request Read or Write
                                    rtuConnection.con);
                        } catch (Exception ex) {
                            Logger.getLogger(GCModbusRTUMasterMain.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                },
                0, 1000);
    }
}
